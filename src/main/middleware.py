import simplejson

class AjaxMiddleware:
    """Middleware for processing incoming JSON data."""

    def process_request(self, request):
        if request.is_ajax() and 'application/json' in request.META['CONTENT_TYPE']:
            request.parsed_body = simplejson.loads(request.body)

        return None
