from django.http import HttpResponseRedirect, QueryDict
from django.shortcuts import resolve_url

from datetime import datetime, timedelta
import unicodedata
import pytz


def normalize_unicode(string):
    return unicodedata.normalize('NFKC', string)


def validate_username(username):
    username = normalize_unicode(username).strip()

    if username.lower() != username.lower().lower():
        raise ValueError('Username is invalid')

    if len(username) < 2:
        raise ValueError('Username too short')

    if len(username) > 30: # FIXME
        raise ValueError('Username is too long')


def validate_password(password):
    password = normalize_unicode(password).strip()

    if len(password) < 6:
        raise ValueError('Password is too short')

    if len(password) > 128:
        raise ValueError('Password is too long')

    #if password.lower() in self.passwords:
    #    return ValueError('Common password')


def unix_to_date(unix):
    return datetime.fromtimestamp(unix, tz=pytz.utc)


def redirect_with_query(path, query=None, *args, **kwargs):
    if query is None:
        query = {}

    url = resolve_url(path, *args, **kwargs)

    if len(query):
        q_dict = QueryDict(mutable=True)
        q_dict.update(query)
        url += '?' + q_dict.urlencode()

    return HttpResponseRedirect(url)


class DateInterval:
    """Class like timedelta, but has a fixed position in time."""

    def __init__(self, start=None, end=None, delta=None):
        """Creates a new instance.

        Must provide at least two of three arguments.

        The delta cannot be less than zero.
        """

        if start is None:
            start = datetime.utcnow()

        if end is None and delta is None:
            end = start

        if end is not None and delta is None:
            delta = end - start
        elif delta is None:
            raise ValueError('No end or delta')

        if delta < timedelta():
            raise ValueError('End before start')

        self._start = start
        self._delta = delta


    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, val):
        self._start = val

    @property
    def end(self):
        return self._start + self._delta

    @end.setter
    def end(self, val):
        self.delta = val - self._start

    @property
    def delta(self):
        return self._delta

    @delta.setter
    def delta(self, val):
        if val < timedelta():
            raise ValueError('End before start')

        self._delta = val

    def __and__(self, obj):
        """Intersection operator for DateInterval objects.

        Returns a timedelta object.

        If the dates overlap, the delta will be non-zero.
        """

        if self.start > obj.end or self.end < obj.start:
            # no overlap
            return timedelta()

        if self.start >= obj.start and self.end <= obj.end:
            # this object is inside the other
            return self.delta

        if self.end >= obj.end and self.start <= obj.end and self.start >= obj.start:
            # this object is to the "right"
            return obj.end - self.start

        if self.start <= obj.start and self.end >= obj.start and self.end <= obj.end:
            # this object is to the "left"
            return self.end - obj.start

        # other object is inside this one
        return obj.delta

    def __repr__(self):
        return 'DateInterval(start={0}, end={1})'.format(
            self.start.astimezone(pytz.utc), self.end.astimezone(pytz.utc))
