from django.contrib.auth.decorators import login_required
from django.db import transaction, IntegrityError
from django.http import JsonResponse, HttpResponse

from throttle.decorators import throttle

from datetime import timedelta, date, time

from main.models import User, Sleep, overlap_exists, get_stats
from . import util

def json_error(msg, status_code=400):
    """Returns a JsonResponse with a custom status code and error message."""

    response = JsonResponse({'error': msg})
    response.status_code = status_code
    return response


def requires(**kwargs):
    """Provides a simple method of validation first level of JSON POST data."""

    def wrapper(wrapped_function):
        def new_func(request):
            if not hasattr(request, 'parsed_body'):
                return HttpResponse('Method not allowed', status=405)
            for key, val in kwargs.items():
                if key not in request.parsed_body:
                    return json_error('Missing argument: ' + key)
                if not isinstance(request.parsed_body[key], val):
                    return json_error('Failed parsing argument: ' + key)

            return wrapped_function(request, request.parsed_body)
        return new_func
    return wrapper


@throttle(zone='ajax')
@requires(username=str)
def username_valid(request, data):
    """Checks if the username is valid or not."""

    try:
        util.validate_username(data['username'])

        if len(User.objects.filter(username=data['username'])) != 0:
            raise ValueError('Username exists')
    except ValueError as e:
        return JsonResponse({'error': True, 'error_string': e.message})

    return JsonResponse({'error': False})


@throttle(zone='ajax')
@requires(password=str)
def password_strength(request, data):
    """Checks potential password strngth."""

    try:
        util.validate_password(data['password'])
    except ValueError as e:
        return JsonResponse({'error': True, 'error_string': e.message})

    return JsonResponse({'error': False})


@throttle(zone='ajax')
@login_required()
@requires(start=int, end=int)
def get_sleeps(request, data):
    """Gets sleeps for the current user."""

    start = util.unix_to_date(data['start'])
    end = util.unix_to_date(data['end'])

    if start >= end:
        return json_error('Invalid dates')

    if end - start > timedelta(days=7):
        return json_error('Range too big')

    results = Sleep. \
        objects. \
        filter(user=request.user, end_time__gte=start, start_time__lte=end). \
        order_by('start_time')

    sleeps = [{'start': s.start_time_stamp * 1000, 'end': s.end_time_stamp * 1000, 'id': s.id} for s in results]

    return JsonResponse(sleeps, safe=False)


@throttle(zone='ajax')
@login_required()
@requires(update=list, create=list, delete=list)
@transaction.atomic()
def save_sleeps(request, data):
    for id in data['delete']:
        Sleep.objects.filter(user=request.user, id=id).delete()

    to_check = []

    for sleep in data['update']:
        start = util.unix_to_date(sleep['start'])
        end = util.unix_to_date(sleep['end'])
        obj = Sleep.objects.get(user=request.user, id=sleep['id'])
        obj.start_time = start
        obj.end_time = end
        obj.full_clean()
        obj.save()
        to_check.append(obj)

    created_ids = {}

    for sleep in data['create']:
        start = util.unix_to_date(sleep['start'])
        end = util.unix_to_date(sleep['end'])
        obj = Sleep.objects.create(user=request.user, start_time=start, end_time=end)
        obj.full_clean()
        obj.save()
        to_check.append(obj)

        created_ids[sleep['i']] = obj.id

    overlap_exists(to_check)

    return JsonResponse({'success': True, 'ids': created_ids})


@throttle(zone='ajax')
@login_required()
@requires(start=str, end=str, cutoff=int)
def get_chart_data(request, data):

    conv = lambda x: date(*list(map(int, x.split('-'))))
    tz = request.user.userprofile.tz

    try:
        start = conv(data['start'])
        end = conv(data['end'])
    except TypeError as e:
        return json_error('Invalid date(s)')

    if end <= start:
        return json_error('End must be greater than start')

    if end - start > timedelta(days=365):
        return json_error('Date range too large')

    cutoff = data['cutoff']

    if cutoff < 0 or cutoff > 23:
        return json_error('Invalid cutoff value')

    cutoff = time(hour=cutoff, tzinfo=tz)
    stats = get_stats(request.user, start, end, cutoff)
    data = []

    for i, (d, interval) in enumerate(stats.items()):
        data.append((i, interval.total_seconds()/60**2))

    return JsonResponse(data, safe=False)

