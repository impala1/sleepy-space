from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db.models import Q, F
from datetime import timedelta, datetime
from django.db import models
from calendar import timegm
from .util import DateInterval
from collections import OrderedDict


import pytz


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    timezone = models.CharField(max_length=64,
        choices=list(zip(pytz.all_timezones, pytz.all_timezones)), default=pytz.utc.zone)


    @property
    def tz(self):
        return pytz.timezone(self.timezone)

    @tz.setter
    def tz(self, val):
        self.timezone = val.zone


    def __repr__(self):
        return 'UserProfile(user={0}, timezone={1})'.format(self.user, self.timezone)


class Sleep(models.Model):
    user = models.ForeignKey(User)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()


    class Meta:
        index_together = [
            ['user', 'start_time'],
            ['user', 'end_time'],
        ]


    @property
    def start_time_stamp(self):
        return timegm(self.start_time.timetuple())


    @property
    def end_time_stamp(self):
        return timegm(self.end_time.timetuple())


    @property
    def duration(self):
        return self.end_time - self.start_time


    @property
    def interval(self):
        return DateInterval(self.start_time, self.end_time)


    def clean(self):
        self.__check_stamp(self.start_time)
        self.__check_stamp(self.end_time)

        if self.duration < timedelta(minutes=15):
            raise ValidationError('The sleep is too short')

        super().clean()


    def __repr__(self):
        return "Sleep(start_time={0}, end_time={1}, user={2})".format(
            self.start_time, self.end_time, self.user.username)


    def __check_stamp(self, stamp):
        if stamp.minute % 15 != 0 or stamp.second != 0 or stamp.microsecond != 0:
            raise ValidationError('Timestamp is not aligned')


def overlap_exists(sleeps):
    """
    Checks if there are any sleeps overlapping for a given user.

    Unforunately forced to use raw SQL since it doesn't look like django
    supports arbitrary self joining."""

    if len(sleeps) == 0:
        return False

    query = Sleep.objects.raw("""
        SELECT s1.*
        FROM main_sleep s1
        JOIN main_sleep s2 ON s2.user_id = s1.user_id
        WHERE
            s2.id != s1.id
            AND
            s1.id IN (""" + ', '.join(['%s'] * len(sleeps)) +  """)
            AND
            (
                (
                    DATE_ADD(s2.start_time, INTERVAL 15 MINUTE) > s1.start_time
                    AND
                    DATE_SUB(s2.start_time, INTERVAL 15 MINUTE) < s1.end_time
                )
                OR
                (
                    DATE_ADD(s1.start_time, INTERVAL 15 MINUTE) > s2.start_time
                    AND
                    DATE_SUB(s1.start_time, INTERVAL 15 MINUTE) < s2.end_time
                )
                OR
                (
                    s1.start_time >= s2.start_time
                    AND
                    s1.end_time <= s2.end_time
                )
                OR
                (
                    s2.start_time >= s1.start_time
                    AND
                    s2.end_time <= s1.end_time
                )
            )""", [x.id for x in sleeps])

    return len(list(query)) != 0


def get_stats(user, start, end, cutoff):
    """Gets stats for the given user between the given dates.

    The input is assumed to be valid.

    Returns a dictionary with dates as keys and timedeltas as values.
    """

    start_time = datetime.combine(start, cutoff)
    end_time = datetime.combine(end, cutoff)

    sleeps = Sleep.objects. \
        filter(end_time__gte=start_time, start_time__lte=end_time, user=user). \
        order_by('start_time')

    day = timedelta(days=1)
    current = DateInterval(datetime.combine(start, cutoff), delta=day)
    data = OrderedDict()

    for i in range(0, (end_time - start_time).days):
        data[(start_time + timedelta(days=i)).date()] = timedelta()

    for sleep in sleeps:
        while current.start <= sleep.end_time:
            try:
                data[current.start.date()] += current & sleep.interval
            except:
                pass
            current.start += day

        current.start -= day # there could be more than one sleep per day

    return data
