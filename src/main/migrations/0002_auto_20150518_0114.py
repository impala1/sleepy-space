# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='sleep',
            index_together=set([('user', 'start_time'), ('user', 'end_time')]),
        ),
    ]
