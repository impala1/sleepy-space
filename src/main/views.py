from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db import transaction, IntegrityError
from django.http import HttpResponseBadRequest, HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages

from throttle.decorators import throttle

from datetime import datetime
from calendar import timegm
import pytz
import csv

from main.models import User, UserProfile, Sleep
from . import util


@throttle(zone='default')
def index(request):
    """Front page."""

    return render(request, 'home.html')


@throttle(zone='default')
def overview(request):
    return render(request, 'overview.html')


def error404(request):
    """Custom 404 error handler."""

    return render(request, 'errors/404.html')


@throttle(zone='login-register')
def register(request):
    """Process user registration."""

    query = {}

    try:
        username = util.normalize_unicode(request.POST['username']).strip()
        password1 = request.POST['password'].strip()
        password2 = request.POST['password2'].strip()
    except KeyError:
        messages.error(request, 'Missing input')
        return redirect('index')

    try:
        util.validate_username(username)
        util.validate_password(password1)

        if password1 != password2:
            raise ValueError('Passwords do not match')
    except ValueError as e:
        messages.error(request, e.message)
        query['register_username'] = username
        return util.redirect_with_query('index', query)

    try:
        with transaction.atomic():
            user = User.objects.create_user(username=username, password=password1)
            profile = UserProfile.objects.create(user=user, timezone='UTC')
            user.save()
            profile.save()

        messages.success(request, 'Successfully registered, please log in')
        query['login_username'] = username
    except IntegrityError as e:
        messages.error(request, 'Such a username already exists')
        query['register_username'] = username

    return util.redirect_with_query('index', query)


@throttle(zone='login-register')
def do_login(request):
    """Authenticates login requests."""

    try:
        username = util.normalize_unicode(request.POST['username']).strip()
        password = request.POST['password'].strip()
    except KeyError:
        return HttpResponseBadRequest('Missing data')

    user = authenticate(username=username, password=password)
    query = {}

    if user is not None:
        if user.is_active:
            login(request, user)
            next_path = request.POST.get('next', '').strip()

            if 'remember' not in request.POST:
                request.session.set_expiry(0)

            return redirect(next_path if next_path else 'home')
        else:
            messages.error(request, 'This account is disabled')
    else:
        messages.error(request, 'Wrong user/password combination')
        query['login_username'] = username

    return util.redirect_with_query('index', query)


@throttle(zone='default')
@login_required()
def do_logout(request):
    """Logs a user out and sends them back to the front page."""

    logout(request)
    messages.success(request, 'Successfully logged out')
    return redirect('index')


@throttle(zone='default')
@login_required()
def home(request):
    """Main page."""

    user_tz = request.user.userprofile.tz
    midnight = datetime.now(pytz.utc).astimezone(user_tz)
    midnight = midnight.replace(hour=0, minute=0, second=0, microsecond=0).astimezone(pytz.utc)

    return render(request, 'user-home.html', {
        'midnight': timegm(midnight.timetuple()),
        'cutoff': range(0, 24)
    })


@throttle(zone='default')
@login_required()
def prefs(request):
    """Preferences page."""

    return render(request, 'prefs.html', {'timezones': pytz.all_timezones})


@throttle(zone='default')
@login_required()
def prefs_save(request):
    """Saves user preferences in the database."""

    try:
        tz = request.POST['timezone']
    except KeyError:
        return HttpResponseBadRequest('Missing data')

    if tz not in pytz.all_timezones:
        return HttpResponseBadRequest('Incorrect data')

    request.user.userprofile.timezone = tz
    request.user.userprofile.save()

    messages.success(request, 'Settings were saved')
    return redirect('prefs')


@throttle(zone='default')
@login_required()
def prefs_save_password(request):
    """Lets users update their password."""

    try:
        password1 = request.POST['password'].strip()
        password2 = request.POST['password2'].strip()
    except KeyError:
        return HttpResponseBadRequest('Missing data')

    try:
        util.validate_password(password1)

        if password1 != password2:
            raise ValueError('Passwords do not match')
    except ValueError as e:
        messages.error(request, e.message)
        return redirect('prefs')

    request.user.set_password(password1)
    request.user.save()
    messages.success(request, 'Password was updated')

    return redirect('prefs')


@throttle(zone='default')
@login_required()
def export(request):
    """Returns all of user's data as a CSV file."""

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=sleep-export.csv'
    data = Sleep.objects.all().order_by('start_time')
    writer = csv.writer(response)
    fmt = lambda x: x.strftime('%Y-%m-%d %H:%M UTC')

    writer.writerow(['Start time', 'End time'])

    for sleep in data:
        writer.writerow([fmt(sleep.start_time), fmt(sleep.end_time)])

    return response
