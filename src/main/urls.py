from django.conf.urls import url

from . import views, api

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^overview/$', views.overview, name='overview'),
    url(r'^login/$', views.do_login, name='login'),
    url(r'^logout/$', views.do_logout, name='logout'),
    url(r'^home/$', views.home, name='home'),
    url(r'^home/prefs/$', views.prefs, name='prefs'),
    url(r'^home/prefs/save/$', views.prefs_save, name='prefs_save'),
    url(r'^home/prefs/save_password/$', views.prefs_save_password, name='prefs_save_password'),
    url(r'^home/export/$', views.export, name='export'),
    url(r'^do_register/$', views.register, name='register'),
    url(r'^api/username_valid/$', api.username_valid),
    url(r'^api/password_strength/$', api.password_strength),
    url(r'^api/get_sleeps/$', api.get_sleeps),
    url(r'^api/save_sleeps/$', api.save_sleeps),
    url(r'^api/get_chart_data/$', api.get_chart_data),
]
