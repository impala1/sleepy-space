from django.contrib import admin

from .models import Sleep, UserProfile

admin.site.register(Sleep)
admin.site.register(UserProfile)
