(function() {
    var app = angular.module('SleepyApi', ['ngCookies']);

    app.config(function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    });

    app.provider('api', function() {
        this.$get = function($http, $cookies) {
            var call = function(method, args, callback) {
                $http({
                    method: 'POST',
                    url: '/api/' + method + '/',
                    data: args,
                    headers: {
                        'X-CSRFToken': $cookies.get('csrftoken')
                    }
                }).success(callback);
            };

            return {
                'call': call
            }
        };
    });
})();
