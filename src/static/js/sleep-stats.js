(function() {
    var app = angular.module('SleepStats', ['SleepyApi', 'angular-flot']);

    app.config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{$').endSymbol('$}');
    });

    app.controller('Chart', function ($scope, api) {
        var c = this;
        var format = 'YYYY-MM-DD';
        var now = moment();

        c.loading = false;
        c.end = now.format(format);
        c.start = now.add(-14, 'days').format(format);

        $scope.myData = [{
            data: [],
            label: 'Sleep (hours)',
            color: '#52A3FF'
        }];

        $scope.myChartOptions = {
            xaxis: {
                tickFormatter: function(n, o) { return moment(c.start, format).add(n+1, 'days').format(format); },
            },
            yaxis: {
                tickSize: 1,
                min: 0,
                autoscaleMargin: 0.1
            },
            series: {
                lines: {
                    show: true
                },
                points: {
                    show: true
                }
            }
        };

        c.datesValid = function() {
            var start = moment(c.start, format);
            var end = moment(c.end, format);

            return start < end && start > end.add(-365, 'days');
        };

        c.enableButton = function() {
            return !c.loading && c.datesValid();
        }

        c.doUpdate = function() {
            c.loading = true;

            api.call('get_chart_data', {
                start: c.start,
                end: c.end,
                cutoff: parseInt(c.cutoff)
            }, function(data) {
                $scope.myData[0].data = data;
                c.loading = false;
            });
        };

        $(window).load(function() {
            c.cutoff = '16';
            c.doUpdate();
        });
    });
})();
