(function() {
    var app = angular.module('RegistrationHelper', ['SleepyApi']);
    var typeTimeout = 300;

    app.config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{$').endSymbol('$}');
    });

    function checkPassword(password) {
        return password && password.length >= 6;
    }

    function checkUsername(username) {
        return username && username.length > 2;
    }

    app.controller('regController', function(api, $scope) {
        var usernameCallHandle;
        var passwordCallHandle;

        $scope.usernameGood = function() {
            return $scope.username && !$scope.username_error;
        }

        $scope.usernameBad = function() {
            return $scope.username && $scope.username_error;
        }

        $scope.passwordGood = function() {
            return $scope.password && !$scope.password_error;
        }

        $scope.passwordBad = function() {
            return $scope.password && $scope.password_error;
        }

        $scope.password2Good = function() {
            return $scope.password2 && !$scope.password2_error;
        }

        $scope.password2Bad = function() {
            return $scope.password2 && $scope.password2_error;
        }

        $scope.$watch('username', function(val) {
            clearTimeout(usernameCallHandle);

            if(!checkUsername(val))
            {
                $scope.username_error = 'Too short';
                return;
            }

            if(val) {
                // reduce number of calls a bit
                usernameCallHandle = setTimeout(function() {
                    api.call('username_valid', {username: val}, function(data) {
                        $scope.username_error = data.error_string;
                    });
                }, typeTimeout);
            }
        });

        $scope.$watch('password', function(val) {
            clearTimeout(passwordCallHandle);

            if(!checkPassword(val))
            {
                $scope.password_error = 'Password is too weak';
                return;
            }


            if(val) {
                // reduce number of calls a bit
                passwordCallHandle = setTimeout(function() {
                    api.call('password_strength', {password: val}, function(data) {
                        $scope.password_error = data.error_string;
                    });
                }, typeTimeout);
            }
        });

        $scope.$watch('password2', function(val) {
            $scope.password2_error = $scope.password === $scope.password2 ? '' : 'Passwords are not identical';
        });
    });
})();
