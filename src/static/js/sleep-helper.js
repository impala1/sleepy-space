(function() {
    var app = angular.module('SleepHelper', ['SleepyApi', 'SleepStats']);

    app.config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{$').endSymbol('$}');
    });

    // saves old copy of sleep if changed, deletes it if they are the same
    var backup_sleep = function(sleep) {
        if(!sleep.old) {
            sleep.old = {
                start: sleep.start,
                end: sleep.end
            }
        } else if(sleep.old.start === sleep.start && sleep.old.end == sleep.end) {
            delete sleep.old;
        }
    };

    var restore_if_same = function(sleep) {
        if(sleep.old && sleep.old.start === sleep.start && sleep.old.end == sleep.end) {
            delete sleep.old;
        }
    }

    app.controller('editController', function(api, $scope, $http) {
        var edit = this;
        var ms_in_min = 1000 * 60;
        var ms_in_hour = ms_in_min * 60;
        var ms_in_day = ms_in_hour * 24;
        var tz_data_loaded = false;
        var drag_sleep_id = false;
        var drag_bar = false;
        var prev_x = false;
        var bar_width_px = $('#sleep-bar').width();
        var bar_left_px = $('#sleep-bar').offset().left;
        var min_adjust_ms = 15 * ms_in_min;
        var resize_sleep_id = false;
        var resize_sleep_end = false;
        var loaded_range;

        edit.pos = new Date().getTime();

        edit.resetPos = function() {
            var now = new Date().getTime();
            edit.goToDate(now);
            edit.setPos(now);
        }

        edit.loadInitial = function(pos) {
            edit.sleeps = [];
            var interval_start = moment(pos).add(-2, 'day').valueOf();
            var interval_end = moment(pos).add(1, 'day').valueOf();
            $scope.loading = true;

            api.call('get_sleeps', {
                    'start': parseInt(interval_start/1000),
                    'end': parseInt(interval_end/1000)
                }, function(data) {
                    loaded_range = {'start': interval_start, 'end': interval_end};
                    edit.sleeps = data;
                    $scope.loading = false;
                }
            );
        }


        edit.loadRange = function(start, end, callback) {
            $scope.loading = true;

            api.call('get_sleeps', {
                    'start': parseInt(start/1000),
                    'end': parseInt(end/1000)
                }, function(data) {
                    loaded_range = {'start': Math.min(start, loaded_range.start), 'end': Math.max(end, loaded_range.end)};

                    // for every loaded sleep
                    loop1: for(var i in data) {
                        var insert = 0;

                        // find place to insert
                        loop2: for(var j in edit.sleeps) {
                            if(data[i].id === edit.sleeps[j].id) {
                                continue loop1;
                            }

                            if(data[i].end > edit.sleeps[j].start) {
                                break;
                            }

                            insert = j;
                        }

                        edit.sleeps.splice(insert, 0, data[i]);
                    }

                    $scope.loading = false;

                    if(callback) {
                        callback();
                    }
                }
            );
        }

        // used by the ngRepeat construct to ignore invisible sleeps for performance
        edit.isVisible = function(sleep) {
            var right = edit.pos;
            var left = right - ms_in_day;
            var res =
                ((sleep.start <= left) && (sleep.end >= left)) ||
                ((sleep.start <= right) && (sleep.end >= right)) ||
                ((sleep.start >= left) && (sleep.end <= right)) ||
                ((sleep.start <= left) && (sleep.end >= right));

            return res;
        }

        edit.setPos = function(pos) {
            if(pos > loaded_range.end) {
                edit.endDragBar();
                edit.loadRange(loaded_range.end, loaded_range.end + ms_in_day * 2, function() { edit.pos = pos; });
            }
            else if(pos - ms_in_day < loaded_range.start) {
                edit.endDragBar();
                edit.loadRange(loaded_range.start - ms_in_day * 2, loaded_range.start, function() { edit.pos = pos; });
            }
            else {
                edit.pos = pos;
            }
        }

        edit.save = function() {
            $scope.loading = true;
            var to_update = [];
            var to_create = [];
            var to_delete = [];

            function conv(t) { return t/1000; }

            for(var i in edit.sleeps) {
                var sleep = edit.sleeps[i];

                if(sleep.id == -1) {
                    to_create.push({
                        i: parseInt(i),
                        start: conv(sleep.start),
                        end: conv(sleep.end)
                    });
                }
                else if(sleep.old) {
                    to_update.push({
                        id: sleep.id,
                        start: conv(sleep.start),
                        end: conv(sleep.end)
                    });
                }
            }

            for(var i in edit.deleted_sleeps) {
                to_delete.push(edit.deleted_sleeps[i].id);
            }

            var params = {
                'update': to_update,
                'create': to_create,
                'delete': to_delete
            };

            api.call('save_sleeps', params, function(data) {
                // delete removed sleeps
                for(var i in edit.sleeps) {
                    if(edit.sleeps[i].old) {
                        delete edit.sleeps[i].old;
                    }
                }

                edit.deleted_sleeps = [ ];

                // set new ids
                for(var i in data.ids) {
                    edit.sleeps[i].id = data.ids[i];
                }

                // make editor available again
                $scope.loading = false;
            });
        }

        edit.calcHrs = function(sleep) {
            var hrs = (sleep.end - sleep.start) / ms_in_hour;
            var str = hrs == 1 ? 'hour' : 'hours';

            return hrs.toFixed(2) + " " + str;
        }

        edit.getMidnightLeft = function() {
            if(!tz_data_loaded) return;
            return moment(edit.pos - ms_in_day / 2) // adjust so that it only jumps when invisible
                .tz(edit.tz)
                .hour(0)
                .minute(0)
                .second(0)
                .millisecond(0)
                .toDate()
                .getTime();
        }

        edit.getMidnightRight = function() {
            if(!tz_data_loaded) return;
            return edit.getMidnightLeft() + ms_in_day;
        }

        edit.getStartPos = function() {
            return edit.pos - ms_in_day;
        }

        edit.calcStartPct = function(sleep) {
            return edit.msToPct(sleep.start - edit.getStartPos());
        }

        edit.calcWidthPct = function(sleep) {
            return edit.msToPct(sleep.end - sleep.start);
        }

        edit.msToPct = function(ms) {
            return (ms * 100.0 / ms_in_day) + '%';
        }

        edit.formatTime = function(time) {
            if(!tz_data_loaded) return;
            return moment(time).tz(edit.tz).format('MMM D @ H:mm');
        }

        edit.formatMidnight = function(date) {
            if(tz_data_loaded) {
                var m = moment(date).tz(edit.tz);
                var right = m.format('MMM D');
                var left = m.subtract(1, 'day').format('MMM D');
                return '⇐ ' + left + ' ... ' + right + ' ⇒';
            }
        }

        edit.pxToMs = function(px) {
            return px * ms_in_day / bar_width_px;
        }

        edit.roundToMinAdjust = function(n) {
            return Math.round(n / min_adjust_ms) * min_adjust_ms;
        }

        edit.goToDate = function(date) {
            var pos = moment(date).tz(edit.tz).add(18, 'hour').valueOf();

            edit.reset();
            edit.loadInitial(pos);
            edit.pos = pos;
        }

        edit.idToIndex = function(id) {
            for(var i in edit.sleeps) {
                if(edit.sleeps[i].id === id) {
                    return parseInt(i);
                }
            }
        }

        edit.createNewSleep = function(e) {
            var time = edit.pos - ms_in_day + edit.pxToMs(e.clientX - bar_left_px);
            time = edit.roundToMinAdjust(time);
            var sleep_left;
            var sleep_right;

            for(var i in edit.sleeps) {
                var sleep = edit.sleeps[i];

                if(sleep.start > time) {
                    sleep_right = sleep;
                    break;
                }

                if(sleep.end < time) {
                    sleep_left = sleep;
                }
            }

            if(sleep_left && sleep_right) {
                if(sleep_right.start - sleep_left.end < 3 * min_adjust_ms) {
                    alert("Not enough space, can't create sleep here.");
                    return;
                }

                // once for initial width, once for minimum distance
                // treat `time` as start time
                if(time + 2 * min_adjust_ms > sleep_right.start) {
                    time = sleep_right.start - 2 * min_adjust_ms;
                }
                else if(time - min_adjust_ms < sleep_left.end) {
                    time = sleep_left.end + min_adjust_ms;
                }
            }

            edit.addSleep({
                start: time,
                end: time + min_adjust_ms,
                id: -1,
                old: {
                    start: 0,
                    end: 0
                }
            });
        }

        edit.addSleep = function(sleep) {
            var pos;

            for(var i in edit.sleeps) {
                if(edit.sleeps[i].start < sleep.end) {
                    pos = 1 + parseInt(i); // WHY IS IT A STRING!?
                }
            }

            pos = pos || edit.sleeps.length;
            edit.sleeps.splice(pos, 0, sleep);
        }

        edit.sleeps = [ ];
        edit.deleted_sleeps = [ ];
        edit.loadInitial(edit.pos);

        $scope.$watch('tz_url', function(url) {
            $http.get(url).success(function(data) {
                moment.tz.load(data);
                tz_data_loaded = true;
                $scope.cal_date = moment().tz(edit.tz).format('YYYY-MM-DD');
            });
        });

        /* individual sleep dragging stuff */

        edit.mouseUp = function(e) {
            if(drag_sleep_id !== false) {
                edit.endDragSleep(e);
            }

            if(drag_bar) {
                edit.endDragBar(e);
            }

            if(resize_sleep_id !== false) {
                edit.endResize(e);
            }
        }

        edit.mouseMove = function(e) {
            if(drag_sleep_id !== false) {
                edit.doDragSleep(e.clientX);
            }

            if(drag_bar) {
                edit.doDragBar(e.clientX);
            }

            if(resize_sleep_id !== false) {
                edit.doResize(e.clientX);
            }

            prev_x = e.clientX;
        }

        edit.startDragSleep = function(id, e) {
            var i = edit.idToIndex(id);
            e.preventDefault();
            e.stopPropagation();
            prev_x = e.clientX;
            drag_sleep_id = i;
        }

        edit.startDragBar = function() {
            drag_bar = true;
        }

        edit.endDragBar = function() {
            drag_bar = false;
        }

        edit.endDragSleep = function(e) {
            e.stopPropagation();
            var sleep = edit.sleeps[drag_sleep_id];
            sleep.start = edit.roundToMinAdjust(sleep.start);
            sleep.end = edit.roundToMinAdjust(sleep.end);
            restore_if_same(sleep);

            prev_x = false;
            drag_sleep_id = false;
        }

        edit.doDragSleep = function(x) {
            var diff = edit.pxToMs(x - prev_x);

            var sleep = edit.sleeps[drag_sleep_id];
            var prev_sleep = edit.sleeps[drag_sleep_id - 1];
            var next_sleep = edit.sleeps[drag_sleep_id + 1];

            backup_sleep(sleep);

            sleep.start += diff;
            sleep.end += diff;

            var duration = sleep.end - sleep.start;

            if(diff < 0 && prev_sleep) {
                if(sleep.start - min_adjust_ms < prev_sleep.end) {
                    sleep.start = prev_sleep.end + min_adjust_ms;
                    sleep.end = sleep.start + duration;
                }
            } else if(diff > 0 && next_sleep) {
                if(sleep.end + min_adjust_ms > next_sleep.start) {
                    sleep.end = next_sleep.start - min_adjust_ms;
                    sleep.start = sleep.end - duration;
                }
            }
        }

        edit.doDragBar = function(x) {
            var diff = edit.pxToMs(x - prev_x);
            edit.setPos(edit.pos - diff);
        }


        /* resize event handling */

        edit.resizeSleepStart = function(id, e) {
            e.preventDefault();
            e.stopPropagation();
            prev_x = e.clientX;
            resize_sleep_id = edit.idToIndex(id);
            resize_sleep_end = 'start';
        }


        edit.resizeSleepEnd = function(id, e) {
            e.preventDefault();
            e.stopPropagation();
            prev_x = e.clientX;
            resize_sleep_id = edit.idToIndex(id);
            resize_sleep_end = 'end';
        }

        edit.endResize = function(e) {
            e.stopPropagation();
            var sleep = edit.sleeps[resize_sleep_id];
            sleep[resize_sleep_end] = edit.roundToMinAdjust(sleep[resize_sleep_end]);
            restore_if_same(sleep);

            prev_x = false;
            resize_sleep_id = false;
            resize_sleep_end = false;
        }

        edit.doResize = function(x) {
            var diff = edit.pxToMs(x - prev_x);
            var sleep = edit.sleeps[resize_sleep_id];
            var duration = sleep.end - sleep.start;

            if(resize_sleep_end == 'start') {
                duration -= diff;
            } else {
                duration += diff;
            }

            var prev_sleep = edit.sleeps[resize_sleep_id - 1];
            var next_sleep = edit.sleeps[resize_sleep_id + 1]

            // too narrow, block action
            if(duration < min_adjust_ms) {
                if(resize_sleep_end == 'start') {
                    sleep.start = sleep.end - min_adjust_ms;
                } else {
                    sleep.end = sleep.start + min_adjust_ms;
                }
            }
            else if(resize_sleep_end == 'start' && prev_sleep) {
                diff = Math.max(prev_sleep.end - sleep.start + min_adjust_ms, diff);
            }
            else if(resize_sleep_end == 'end' && next_sleep) {
                diff = Math.min(next_sleep.start - sleep.end - min_adjust_ms, diff);
            }

            backup_sleep(sleep);
            sleep[resize_sleep_end] += diff;
        }

        edit.deleteSleep = function(id) {
            var i = edit.idToIndex(id);
            var sleep = edit.sleeps.splice(i, 1)[0];

            if(sleep.id != -1) {
                if(sleep.old) {
                    sleep.start = sleep.old.start;
                    sleep.end = sleep.old.end;
                    delete sleep.old;
                }

                edit.deleted_sleeps.push(sleep);
            }
        }

        edit.canReset = function() {
            for(var i in edit.sleeps) {
                if(edit.sleeps[i].old) {
                    return true;
                }
            }

            return edit.deleted_sleeps.length !== 0;
        }

        edit.reset = function() {
            var to_delete = [];

            for(var i in edit.sleeps) {
                if(edit.sleeps[i].old) {
                    if(edit.sleeps[i].old.start === 0) {
                        to_delete.push(i);
                    }
                    else {
                        edit.sleeps[i] = edit.sleeps[i].old;
                    }
                }
            }

            for(var i in to_delete) {
                edit.sleeps.splice(to_delete[i], 1);
            }

            for(var i in edit.deleted_sleeps) {
                edit.sleeps.push(edit.deleted_sleeps[i]);
            }

            edit.deleted_sleeps = [ ];

            edit.sleeps.sort(function(a, b) {
                return a.start - b.start;
            });
        }
    });
})();
