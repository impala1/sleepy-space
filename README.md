# README #

Sleepy.space is a very basic online sleep tracker. Sleep is the only thing it tracks. No ads, tracking pixels/scripts or unnecessary disclosure of personal details is supported.

**[Visit the live site](http://sleepy.space/)**.

![2015-06-08_19:52:54_1580x1028.png](https://i.imgur.com/aIPUK3q.png)

## Issues ##

If you find any issues, please [submit them to the bugtracker](https://bitbucket.org/impala/sleepy-space/issues?status=new&status=open). Anonymous contributions are welcome.

## Installation ##

If you are not satisfied with the public install, you are free to run it on your own servers. Since this method isn't really supported (and I don't expect anyone to actually do this), the instructions are light on details.

The dependencies are:

- Python 3.3+
- MySQL/MariaDB - only one dialect is supported due to django's ORM being weak

#### Instructions ####

1. You will probably want to install and use [virtualenv](https://pypi.python.org/pypi/virtualenv).
2. Run the `install.sh` in the `scripts` directory. This will install the dependencies for you. Depends on [pip](https://pypi.python.org/pypi/pip/) and [bower](http://bower.io/).
3. Go to the `src` folder
4. Set up your database `config.py`
5. Run `python manage.py migrate`
6. Run `python manage.py createsuperuser` and create your main account
7. Run `python manage.py runserver` and access the site at localhost:8000