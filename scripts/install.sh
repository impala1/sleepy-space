#!/bin/sh

ROOT_PATH=$(realpath "$(dirname $(realpath $0))/..")
SRC_PATH=$(realpath "${ROOT_PATH}/src")

msg()
{
    echo ">>> $@"
    echo ""
}


install_bower_deps()
{
    msg "Installing bower dependencies"
    cd $ROOT_PATH
    bower install

    if [ $? -ne 0 ]; then
        msg "Installing bower dependencies failed, exiting"
        exit 1
    else
        msg "Successfully installed bower dependencies"
    fi
}


install_python_deps()
{
    msg "Installing python dependencies"
    packages="${ROOT_PATH}/python_deps.txt"
    pip install -r $packages

    if [ $? -ne 0 ]; then
            msg "Installing python dependencies failed, exiting"
            exit 1
    else
            msg "Successfully installed python dependencies"
    fi
}


install_config_from_template()
{
    src_file="${SRC_PATH}/config.py.template"
    dest_file="${SRC_PATH}/config.py"

    if [ -f "$dest_file" ]; then
        msg "${dest_file} already exists, not overwriting"
    else
        msg "Copying template configuration file"
        cp "${src_file}" "${dest_file}"
        msg "Edit the ${dest_file} file to set up your database and secret key"
    fi
}


if ! type pip > /dev/null; then
    msg "Command 'pip' is not installed. Cannot proceed."
    exit 1
fi

if ! type bower > /dev/null; then
    msg "Command 'bower' is not installed. Cannot proceed."
    exit 1
fi


install_bower_deps
install_python_deps
install_config_from_template

msg "The dependencies are now installed. Edit the conf.py file and initiate the database if you have not done so already."
